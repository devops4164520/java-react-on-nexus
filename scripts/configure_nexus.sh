#!/bin/bash

# Check if both username and password are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <username> <password>"
    exit 1
fi

export NEXUS_USERNAME=$1
export NEXUS_PASSWORD=$2

# Function to make a POST request
post_request() {
    curl -X 'POST' -u $NEXUS_USERNAME:$NEXUS_PASSWORD \
      'http://localhost:8081/service/rest/v1/'$1 \
      -H 'accept: application/json' \
      -H 'Content-Type: application/json' \
      -H 'NX-ANTI-CSRF-TOKEN: 0.33095598435515583' \
      -H 'X-Nexus-UI: true' \
      -d "$2"
}

# Create blob storage for npm-hosted repository
post_request "blobstores/file" '{"path": "/opt/sonatype-work/nexus3/blobs/npm-BlobStore", "name": "npm-BlobStore"}'

# Create npm-repository
post_request "repositories/npm/hosted" '{
  "name": "npm-hosted",
  "online": true,
  "storage": {
    "blobStoreName": "npm-BlobStore",
    "strictContentTypeValidation": true,
    "writePolicy": "allow_once"
  },
  "component": {
    "proprietaryComponents": true
  }
}'

# Create maven hosted repository
post_request "repositories/maven/hosted" '{
  "name": "maven-hosted",
  "online": true,
  "storage": {
    "blobStoreName": "default",
    "strictContentTypeValidation": true,
    "writePolicy": "allow_once"
  },
  "cleanup": {
    "policyNames": []
  },
  "component": {
    "proprietaryComponents": true
  },
  "maven": {
    "versionPolicy": "SNAPSHOT",
    "layoutPolicy": "STRICT",
    "contentDisposition": "INLINE"
  }
}'

# Create role permissions
roles=(
    '{"id": "nx-java", "name": "nx-java", "description": "", "privileges": ["nx-repository-view-maven2-maven-snapshots-*"], "roles": []}'
    '{"id": "nx-npm", "name": "nx-npm", "description": "", "privileges": ["nx-repository-view-npm-npm-hosted-*"], "roles": []}'
)

for role in "${roles[@]}"; do
    post_request "security/roles" "$role"
done

# Create project teams
teams=(
    '{"userId": "team1", "firstName": "project-team1", "lastName": "project-team1", "emailAddress": "project-team1@example.com", "password": "team1-pass", "status": "active", "roles": ["nx-npm"]}'
    '{"userId": "team2", "firstName": "project-team2", "lastName": "project-team2", "emailAddress": "project-team2@example.com", "password": "team2-pass", "status": "active", "roles": ["nx-java"]}'
    '{"userId": "global", "firstName": "global-team2", "lastName": "global-team2", "emailAddress": "global-team2@example.com", "password": "global-pass", "status": "active", "roles": ["nx-java", "nx-npm"]}'
)

for team in "${teams[@]}"; do
    post_request "security/users" "$team"
done
