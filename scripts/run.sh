#!/bin/bash

# Check if both username and password are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <username> <password>"
    exit 1
fi

export NEXUS_USERNAME=$1
export NEXUS_PASSWORD=$2

# save the artifact details in a json file
curl -u $NEXUS_USERNAME:$NEXUS_PASSWORD -X GET 'http://localhost:8081/service/rest/v1/components?repository=npm-hosted&sort=version' | jq "." > artifact.json

# grab the download url from the saved artifact details using 'jq' json processor tool
artifactDownloadUrl=$(jq '.items[].assets[].downloadUrl' artifact.json --raw-output)

# fetch the artifact with the extracted download url using 'wget' tool
wget $artifactDownloadUrl

# unpack the file
tar -zxvf bootcamp-node-project-1.0.0.tgz

# move into package and install modules
cd package/ && npm install

# Run application
node server.js