# Java-Maven-NodeJS-On-Nexus

## Overview
The `Java-Maven-NodeJS-On-Nexus` project is a comprehensive setup for deploying Node.js and Java applications using Nexus repositories. It streamlines the process of managing dependencies and deploying artifacts by leveraging npm and Maven hosted repositories on a Nexus server.

## Automated setup using NEXUS REST APIs

1. **Created npm Hosted Repository**: a new npm hosted repository with a dedicated blob store to store Node.js artifacts.

2. **User Access Management**: Created a Nexus user for `Project team 1` to grant access to the npm repository.

3. **Publish Node.js Package**: Built and published the Node.js tar package to the npm repository using the designated team 1 user account.

4. **Created Maven Hosted Repository**: Establish a Maven hosted repository to manage Java artifacts.

5. **User Access Management**: Created a Nexus user for `Project team 2` to enable access to the Maven repository.

6. **Publish Java Jar File**: Built and published the Java Jar file to the Maven repository using the designated team 2 user account.

7. **Server Access Configuration**: Created a new global user for the nexus server with access privileges to both repositories.

8. **Deployment on Droplet Server**: Utilized the Nexus REST API to fetch the download URL information for the latest Node.js app artifact and deploy on server.

![deployment_outline](deployment_outline.png)

## Features
- Seamless deployment of Node.js applications using npm hosted repository.
- Efficient management of Java applications via Maven hosted repository.
- Integration with Nexus server for centralized artifact management.
- Access control for different project teams ensuring secure deployment.

## Usage
1. Ensure that all necessary dependencies and configurations are set up correctly.

2. Build the Node.js and Java application using appropriate commands.

    **For Node.js** - Run `npm pack`

    **For Java app** - Configure nexus credentials in `~/.m2/settings.xml` file and run `mvn package`.

3. Run `nexus_provision.sh` to provision VM on digital ocean, which also runs  `nexus_startup.sh` script.

4. On nexus server run the `configure_nexus.sh` to create blobs, user accounts and repositories.

5. Publish the built artifacts to the respective Nexus repositories. Using commands below:

    **Node.js** - `npm publish --registry=http://localhost:8081/repository/npm-hosted/ bootcamp-node-project-1.0.0.tgz`

    **Java App** - `mvn deploy`

6. Publish the built artifacts to the respective Nexus repositories.
7. Access the Droplet server and utilize the Nexus REST API to fetch and deploy the latest artifacts.
8. Execute necessary commands to untar the Node.js artifact and run it on the server.